\documentclass[a4paper]{article}

\usepackage{amsmath,amssymb,bbm,enumitem,etoolbox,hyperref,mathtools,multicol,theorem,stmaryrd,version}

\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage[french]{babel}

\def \CC {\mathbbm C}
\def \KK {\mathbbm K}
\def \NN {\mathbbm N}
\def \QQ {\mathbbm Q}
\def \RR {\mathbbm R}
\def \UU {\mathbbm U}
\def \ZZ {\mathbbm Z}

\begin{document}
\title{Maths en sup - idées ambitieuses}
\maketitle

\textbf{\large{Notes}}

On note - les items déjà au programme en terminale. Certes, on ne peut
pas compter dessus à 100\%, mais les élèves ont déjà reçu une dose
propédeutique, donc on peut penser qu'ils représentent une charge
moins importante que ce que l'on pourrait craindre de prime abord. Les
autres sont notés +. Je n'ai regardé que le programme de la
spécialité: certaines choses sont vues dans l'option maths expertes,
mais on ne peut pas compter que tous nos étudiants l'auront prise,
même si on peut rêver!

L'objectif est de donner aux étudiants une base \textbf{pratique}
solide : c'est pourquoi certains espaces sont introduits avec une
notation et la construction de l'intégrale est repoussée en spé. À ce
moment-là, l'intégrale de Lebesgue devrait permettre de construire les
intégrales (sur les segments et impropres) et fusionner un certain
nombre de chapitres pour le moment séparés - ce qui libère le temps
nécessaire pour cette théorie.

\part*{Généralités}

\section{Raisonnement et vocabulaire}

\subsection{Rudiments de logique}

\begin{itemize}
\item[$-$] connecteurs : négation, et, ou, implication, équivalence ;
\item[$-$] quantificateurs
\item[+] stratégies de preuve : par contraposée, implications
  réciproques, implications circulaires, par l'absurde, par
  analyse-synthèse (certaines déjà vues, pas toutes).
\end{itemize}

\subsection{Ensembles}

\begin{itemize}
\item[$-$] appartenance ;
\item[$-$] union, intersection, passage au complémentaire ;
\item[+] inclusion (au programme en terminale), ensemble des parties
  (pas vu en terminale).
\end{itemize}

\subsection{Applications}

\begin{itemize}
\item[+] définition ;
\item[+] injection, surjection, bijection (vues en terminale en situation) ;
\item[+] notations $f_\star$ et $f^\star$ pour les applications sur les parties ;
\item[+] indicatrices $\mathbbm{1}_*$ ;
\item[$-$] composition (vue en terminale en situation).
\end{itemize}

\subsection{Relations sur un ensemble}

\begin{itemize}
\item[+] réflexive, symétrique, antisymétrique, transitive ;
\item[+] ordre total ou non ;
\item[+] équivalence.
\end{itemize}

\section{Nombres}

\subsection{Entiers}

\begin{itemize}
\item[+] $\NN$, $\ZZ$, inégalités, multiples, diviseurs ;
\item[+] factorielle, coefficients binomiaux, triangle de Pascal ;
\item[$-$] principe de récurrence et donc preuves par récurrence ;
\item[+] $\Sigma k$, $\Sigma k^2$, $\Sigma k^3$ ;
\item[+] division euclidienne.
\end{itemize}

\subsection{Réels}

\begin{itemize}
\item[+] $\RR$, relation d'ordre, compatibilité avec les opérations,
  intervalles ;
\item[+] valeur absolue, inégalité triangulaire, $|x-a|leqslant b$ ;
\item[+] parties minorées, majorées, bornées ; propriété de la borne
  supérieure, partie entière.
\end{itemize}

\subsection{Complexes}

\begin{itemize}
\item[+] définition avec des opérations de calcul dans $\RR^2$ ;
\item[+] $\Sigma$ et $\prod$ ; sommes télescopiques ; formule du binôme,
  $a^n-b^n$ ;
\item[+] sommes doubles dont triangulaires et doubles sommes ;
\item[+] sommes de progressions arithmétiques et géométriques ;
\item[+] exponentielle complexe et trigonométrie (la trigonométrie est
  vue en terminale, mais sans les nombres complexes!).
\end{itemize}

\section{Dénombrement}

\subsection{Ensembles finis}

\begin{itemize}
\item[+] notion de cardinal, inégalités par injections et surjections ;
\item[+] cardinal d'une union disjointe, du complémentaire, d'un
  produit cartésien, des applications, des applications injectives,
  des parties.
\end{itemize}

\subsection{Ensembles dénombrables}

\begin{itemize}
\item[+] définition par bijection avec $\NN$ ; parties non finies de $\NN$ ;
\item[+] le produit de deux ensembles dénombrables est dénombrable ;
\item[+] pas de bijection entre $M$ et $\mathcal P(M)$ pour tout
  ensemble $M$.
\end{itemize}

\part*{Analyse}

\section{Suites}

\subsection{Généralités}

\begin{itemize}
\item[$-$] définition ;
\item[$-$] caractérisations de la monotonie dans le cas réel (par
  différence toujours, par quotient sous hypothèses) ;
\item[$-$] bornitude [sic?].
\end{itemize}

\subsection{Limites}

\begin{itemize}
\item[+] les trois définitions, dont convergence avec sa linéarité, la
  caractérisation par les parties réelles et imaginaires, elles sont
  bornées ;
\item[$-$] produit et quotient ;
\item[$-$] passage à la limite dans les inégalités ; théorème de
  convergence monotone ; théorème de convergence par encadrement ;
\item[$-$] suites adjacentes ;
\end{itemize}

\subsection{Comparaisons}

\begin{itemize}
\item[+] relations de comparaison avec notations de Landau ;
\item[+] croissances comparées ;
\end{itemize}

\subsection{Exemples}

\begin{itemize}
\item[+] suites arithmético-géométriques ;
\item[$-$] suites récurrentes linéaires d'ordre 2 à coefficients constants ;
\end{itemize}

\section{Séries}

\subsection{Familles sommables}

\begin{itemize}
\item[+] définition pour les familles de réels positifs par borne sup
  des sommes finies, donc on traite les familles infinies,
  commutativité, seul un nombre dénombrable de termes sont non nuls ;
\item[+] définition pour les familles de réels via les familles $x^+$
  et $x^-$, commutativité, seul un nombre dénombrable de termes sont
  non nuls, notation de $\ell^1(I)$ où $I$ est l'ensemble des indices,
  structure vectorielle et linéarité de la somme ;
\item[+] extension aux familles complexes.
\end{itemize}

\subsection{Séries numériques}

\begin{itemize}
\item[+] vocabulaire des sommes partielles ;
\item[+] définition de la convergence absolue par sommabilité, de la
  convergence tout court ; linéarité ;
\item[+] convergence absolue par domination, par équivalence ;
\item[+] séries géométriques, critère de d'Alembert ;
\item[+] séries de Riemann admises temporairement ;
\item[+] séries télescopiques ;
\item[+] critère spécial pour les séries alternées.
\end{itemize}

\section{Fonctions de la variable réelle}

\subsection{Opérations}

\begin{itemize}
\item[+] définition de la sommme, du produit, du quotient, des
  inégalités [exemple d'ordre non total].
\end{itemize}

\subsection{Fonctions usuelles}

\begin{itemize}
\item[+] définition des fonctions puissances ;
\item[$-$] logarithme, exponentielle ;
\item[$-$] Fonctions trigonométriques ;
\item[+] Fonction hyperboliques ;
\item[+] Inverses des précédentes.
\end{itemize}

\subsection{Limites}

\begin{itemize}
\item[+] définitions ; unicité ; linéarité ;
\item[$-$] produit, composition ;
\item[$-$] passage à la limite dans les inégalités ; théorème de la limite
  monotone ; théorème de la convergence par encadrement ;
\item[+] relations de comparaison avec notations de Landau ;
\item[+] caractérisation séquentielle ;
\item[+] croissances comparées.
\end{itemize}

\subsection{Continuité}

\begin{itemize}
\item[+] ponctuelle ;
\item[+] caractérisation séquentielle ;
\item[$-$] sur un domaine ; notation $\mathcal C^0(I)$ et structure vectorielle ;
\item[$-$] prolongement par continuité ;
\item[+] produit, composition ;
\item[$-$] théorème des valeurs intermédiaires pour les fonctions à
  valeurs réelles (admis) ;
\item[+] Continuité de la réciproque d'une fonction continue
  strictement monotone (bien: réelle à valeurs réelles!) (admis).
\end{itemize}

\subsection{Dérivabilité}

\begin{itemize}
\item[$-$] nombre dérivé, fonction dérivée ;
\item[$-$] la dérivabilité implique la continuité ;
\item[$-$] linéarité, produit, quotient, composition ;
\item[+] dérivée de la réciproque ;
\item[$-$] dérivée et étude des variations (caractérisation de la
  monotonie, condition nécessaire d'extremum) ;
\item[+] théorème de Rolle ;
\item[+] inégalité et égalité des accroissements finis ;
\item[+] fonction lipschitzienne ;
\item[+] théorème de limite de la dérivée.
\end{itemize}

\subsection{Classe $\mathcal C^k$}

\begin{itemize}
\item[+] définition, structure vectorielle, notation $\mathcal C^k$ ;
\item[+] produit (formule de Leibniz) ;
\item[+] quotient, composition, réciproque.
\end{itemize}

\subsection{Convexité}

\begin{itemize}
\item[$-$] définition ;
\item[$-$] caractérisation par la croissance de la dérivée ;
\item[$-$] caractérisation par la positivité de la dérivée seconde.
\end{itemize}

\subsection{Intégration}

\begin{itemize}
\item[$-$] définition comme application sur les fonctions continues sur
  un segment, linéaire, croissante, positive et définie ; existence
  admise en sup ;
\item[+] domination $|\int.|\leqslant\int|.|$ ;
\item[$-$] relation de Chasles ;
\item[+] extension au cas $a\geqslant b$ de la notation, ajustement
  des diverses propriétés ;
\item[$-$] primitives: définition, primitivation par intégration,
  notation $[F(x)]_{x=a}^{x=b}$ ;
\item[+] si une fonction est de classe $\mathcal C^1$, elle est une
  primitive de sa dérivée ;
\item[$-$] intégration par parties ;
\item[+] changement de variable ;
\item[+] formule de Taylor avec reste intégral.
\end{itemize}

\subsection{Développements limités}

\begin{itemize}
\item[+] définition, unicité, troncature ;
\item[+] formule de Taylor-Young ;
\item[+] développements limités usuels en zéro ;
\item[+] opérations: combinaison linéaire, produit, quotient ;
\item[+] primitivation ;
\item[+] composition en pratique sans théorie.
\end{itemize}

\part*{Probabilités}

\section{Espaces probabilisés}

\begin{itemize}
\item[+] définition des tribus, exemples: grossière, discrète, trace
  sur une partie ($\mathcal T\cap A$) ;
\item[+] vocabulaire sur les issues, l'univers, les évènements, les
  évènements contraires, impossibles, les systèmes complets finis et
  dénombrables ;
\item[+] définition des probabilités, croissance, probabilité de
  l'union de deux évènements, du contraire.
\end{itemize}

\section{Probabilités conditionnelles}

\begin{itemize}
\item[$-$] définition, probabilité $P_B$ ;
\item[+] probabilités composées ;
\item[$-$] formule des probabilités totales ;
\item[+] formule de Bayes ;
\item[$-$] évènements indépendants, mutuellement indépendants.
\end{itemize}

\section{Variables aléatoires discrètes}

\begin{itemize}
\item[+] définition ;
\item[$-$] notations pour les évènements associés ;
\item[+] système complet dénombrable associé ;
\item[+] loi ;
\item[+] exemple de l'indicatrice d'un évènement.
\end{itemize}

\section{Lois usuelles}

\begin{itemize}
\item[$-$] $B(p)$, $B(n,p)$ et $U(n)$ ;
\item[+] $G(p)$ et $\mathcal P(\lambda)$.
\end{itemize}

\section{Couples de variables aléatoires}

\begin{itemize}
\item[+] définition, loi conjointe, lois marginales, liens ;
\item[+] loi conditionnelle de $Y$ sachant $X=x$ ;
\item[+] indépendance, indépendance admise de $f(X)$ et $g(Y)$,
  indépendance mutuelle ;
\item[$-$] $B(n,p)$ vue comme somme de $n$ $B(p)$ indépendantes.
\end{itemize}

\section{Espérance}

\begin{itemize}
\item[+] définition (vue en terminale pour les univers finis), via:
  $E(\sum_jy_j\mathbbm{1}_{A_j})=\sum_jy_jP(A_j)$, ce qui utilise des
  systèmes complets d'évènements et leurs raffinements, similaires à
  ce qui est fait actuellement avec les fonctions en escalier et les
  subdivisions ;
\item[+] exemples concrets avec l'indicatrice d'un évènement et les
  lois usuelles ;
\item[+] notation $L^1(\Omega, P)$ et structure vectorielle ;
\item[+] théorème de transfert ;
\item[$-$] $E(XY)=E(X)E(Y)$ sous hypothèses ;
\item[+] inégalité de Markov.
\end{itemize}

\section{Variance}

\begin{itemize}
\item[$-$] définition, et écart-type (vus en terminale pour les univers
  finis) ;
\item[+] exemples concrets avec l'indicatrice d'un évènement et les
  lois usuelles ;
\item[+] formule de König-Huygens ;
\item[$-$] relation $V(aX+b)=a^2V(X)$ ;
\item[+] notation $L^2(\Omega, P)$ et structure vectorielle, produit de
  deux éléments de $L^2$ ;
\item[$-$] inégalité de Bienaymé-Tchebychev ;
\item[$-$] loi faible des grands nombres.
\end{itemize}

\part*{Algèbre}

\section{Calcul matriciel}

\subsection{Généralités}

\begin{itemize}
\item[+] définition comme tableaux de nombres ; notation
  $\mathcal M_{n,p}(\KK)$ avec seulement les réels et les complexes ;
\item[+] structure vectorielle ;
\item[+] transposition ;
\item[+] matrices augmentées.
\end{itemize}

\subsection{Produit}

\begin{itemize}
\item[+] définition du produit [c'est l'occasion de voir un produit nul
  sans facteur nul], des matrices inversibles ;
\item[+] cas particulier du produit par une matrice colonne à droite,
  avec son interprétation comme un calcul d'une combinaison linéaire
  des colonnes ;
\item[+] opérations élémentaires sur les lignes et les colonnes vues
  comme des produits à gauche ou à droite par des matrices inversibles
  simples et explicites ;
\end{itemize}

\subsection{Lien avec les systèmes}

\begin{itemize}
\item[+] correspondance entre matrices et systèmes affines ;
\item[+] noyau d'une matrice et structure vectorielle avec la notation
  $\mathrm{Ker}(A)$ ;
\item[+] structure générale des solutions d'un système affine ;
\item[+] transformation d'un système affine en système linéaire par
  matrice augmentée.
\end{itemize}

\subsection{Échelonnement}

\begin{itemize}
\item[+] définition d'une matrice échelonnée, d'une matrice échelonnée
  réduite ;
\item[+] existence d'une unique matrice échelonnée réduite telle que
  $A=PE$ avec $P$ inversible ;
\item[+] détermination du noyau d'une matrice échelonnée réduite par
  simple lecture ;
\end{itemize}

\section{Algèbre linéaire}

\subsection{Espaces vectoriels}

\begin{itemize}
\item[+] définition, sous-espaces ;
\item[+] exemples : $\KK^n$, $E^X$ avec $E$ un espace vectoriel et $X$
  un simple ensemble, $\ell^1(I)$, $L^1(\Omega, P)$, $L^2(\Omega, P)$,
  $\mathcal C^k(I)$...
\item[+] paire de sous-espaces supplémentaires.
\end{itemize}

\subsection{Familles de vecteurs}

\begin{itemize}
\item[+] définition de ``presque tous nuls'' pour une famille de
  scalaires ; combinaisons linéaires finies d'une famille quelconque
  de vecteurs ;
\item[+] familles libres, génératrices, bases ;
\item[+] théorème de la base incomplète: étant données une famille libre
  $\mathcal L$ et une famille génératrice $\mathcal G$, il existe une
  partie $G\subset\mathcal G$ telle que $\mathcal L\cup G$ est une
  base -- cet énoncé couvre à la fois le côté base incomplète et base
  extraite ; il donne l'existence de bases, de supplémentaires.
\end{itemize}

\subsection{Dimension finie}

\begin{itemize}
\item[+] définition par l'existence d'une famille génératrice finie ;
\item[+] si une base a pour cardinal $n$, toute famille de $m>n$
  vecteurs est liée ; ce qui a pour conséquence que toutes les bases
  ont le même cardinal ;
\item[+] dimensions des espaces $\KK^n$, matriciels ;
\item[+] caractérisation des bases par leur cardinal et le fait d'être
  libre ou génératrice ;
\item[+] rang d'une famille ;
\item[+] dimension des sous-espaces, caractérisation de l'espace par
  sa dimension parmi les sous-espaces ;
\item[+] formule de Grassmann.
\end{itemize}

\subsection{Applications linéaires}

\begin{itemize}
\item[+] définition, noyau, image ; vocabulaire iso- endo- auto- ;
\item[+] notation $\mathcal L(E, F)$ et structure vectorielle ;
\item[+] isomorphisme entre tout supplémentaire du noyau et l'image ;
\item[+] exemples : projections et symétries ;
\item[+] théorème du rang, sous l'hypothèse que l'espace de départ est
  de dimension finie ;
\item[+] pour les endomorphismes en dimension finie, équivalence entre
  injectif, surjectif et bijectif.
\end{itemize}

\subsection{Espaces préhilbertiens}

\begin{itemize}
\item[+] définitions du produit scalaire, cas réel et complexe (le cas
  réel dans le plan et l'espace est vu en terminale) ;
\item[+] identités de polarisation ;
\item[+] inégalité de Cauchy-Schwarz (avec cas d'égalité) ;
\item[+] norme associée à un produit scalaire, avec les propriétés
  caractéristiques des normes ;
\item[+] exemples avec $\KK^n$ et $L^2(\Omega, P)$ ;
\item[+] $\ell^2(I)$ ;
\item[$-$] familles orthogonales, orthonormales ;
\item[+] théorème de Pythagore ;
\item[+] supplémentaire orthogonal si existence, avec inégalité de
  Bessel et projection orthogonale ;
\item[+] projection orthogonale sur un sous-espace de dimension finie,
  et algorithme de Gram-Schmidt ;
\item[+] définition des espaces euclidiens.
\end{itemize}

\subsection{Polynômes}

\begin{itemize}
\item[+] $\KK[X]$ construit comme les fonctions $\NN\rightarrow\KK$
  nulles presque partout - d'où la structure vectorielle ;
\item[+] définition de $X$ et la base canonique ;
\item[+] produit et composition de polynômes ;
\item[+] degré d'un polynôme (convention $-\infty$ pour le polynôme
  nul), d'une somme, d'un produit, d'une composée ;
\item[+] sous-espaces $\KK_n[X]$ ;
\item[+] fonction polynomiale associée ;
\item[+] division euclidienne ;
\item[+] dérivation formelle, linéarité, dérivée $k$-ème, formule de
  Taylor ;
\item[+] racines d'un polynôme: définition, caractérisation par la
  divisibilité, multiplicité (et sa caractérisation différentielle) ;
\item[+] notion de polynôme scindé, théorème de d'Alembert-Gauss
  (admis), relation coefficients-racines uniquement pour la somme et
  le produit (cas particulier du degré $2$) ;
\item[+] décomposition des polynômes dans $\RR[X]$ et $\CC[X]$.
\end{itemize}

\end{document}